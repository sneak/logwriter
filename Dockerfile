# focal 2020.01 dev
FROM ubuntu@sha256:d050ed7278c16ff627e4a70d7d353f1a2ec74d8a0b66e5a865356d92f5f6d87b

ENV DEBIAN_FRONTEND noninteractive

#ARG UBUNTU_MIRROR=http://archive.ubuntu.com/ubuntu
#RUN echo "deb $UBUNTU_MIRROR focal main universe restricted multiverse" > /etc/apt/sources.list.new && \
#echo "deb $UBUNTU_MIRROR focal-updates main universe restricted multiverse" >> /etc/apt/sources.list.new && \
#echo "deb $UBUNTU_MIRROR focal-security main universe restricted multiverse" >> /etc/apt/sources.list.new && \
#echo "deb $UBUNTU_MIRROR focal-backports main universe restricted multiverse" >> /etc/apt/sources.list.new && \
#mv /etc/apt/sources.list.new /etc/apt/sources.list

COPY ./root /tmp/rootoverlay
COPY ./run.sh /tmp/run.sh

# we only install essential system packages here
ARG SYSTEM_PACKAGES="build-essential ca-certificates openssh-server git golang rsync runit"

RUN \
    apt update && \
    apt -y upgrade && \
    apt -y install $SYSTEM_PACKAGES && \
    rsync -avP /tmp/rootoverlay/ / && \
    rm -rf /tmp/rootoverlay && \
    chmod +x /etc/service/*/run && \
    go get -v -u git.eeqj.de/sneak/runsvinit && \
    cp /root/go/bin/runsvinit /usr/local/sbin/runsvinit && \
    rm -r /root/go && \
    /bin/bash /tmp/run.sh && \
    rm -f /tmp/run.sh

# /var/log is distro-default stuff, /var/logs is ours.
VOLUME /var/logs

EXPOSE 514
EXPOSE 514/udp
# RELP:
EXPOSE 20514

CMD ["/usr/local/sbin/runsvinit"]

#!/bin/bash

set -x

PKGS="
    rsyslog
    rsyslog-czmq
    rsyslog-doc
    rsyslog-elasticsearch
    rsyslog-gnutls
    rsyslog-gssapi
    rsyslog-hiredis
    rsyslog-kafka
    rsyslog-mongodb
    rsyslog-mysql
    rsyslog-openssl
    rsyslog-pgsql
    rsyslog-relp
    golang
    git
"

export DEBIAN_FRONTEND=noninteractive
apt update
apt install -y $PKGS

git clone https://github.com/gliderlabs/logspout.git /usr/local/src/logspout
# current master as of 20200303
LS_VERSION="8617629f8caffe866dece71c60967349f23c8d2d"
cd /usr/local/src/logspout
git checkout $LS_VERSION
go get -v
go build -o /usr/local/bin/logspout


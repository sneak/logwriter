# sneak/logwriter

[![Build Status](https://drone.datavi.be/api/badges/sneak/logwriter/status.svg)](https://drone.datavi.be/sneak/logwriter)


Container to run logspout + rsyslog and ingest docker logs and write them to
disk as json.

# Use Case

I have a single box that runs a bunch of containers, and I want to log all
those containers to datestamped logfiles as json.

All of the other ways of commonly doing this involve 47 moving parts, so I
made this container.  It uses logspout to ingest, and feeds into an rsyslog
in the same container, which then writes them to disk as json.

Figuring out how to use them is another matter; at least now they're saved
in a way that is easy to tarball up and compress and scp somewhere else on a
daily cronjob.

# example

```
docker \
    run \
    -d \
    --restart always \
    -v /srv/z/archive/logs/current:/var/logs \
    -v /var/run/docker.sock:/var/run/docker.sock \
    sneak/logwriter@sha256:3229118cd23470206ffa0c1de37762ae9d5113e2720bdcf2c8576b5635763893
```

default: build

build:
	docker build \
		--build-arg UBUNTU_MIRROR="http://ubuntu.datavi.be/ubuntu" \
		-t sneak/logwriter \
		.

run:
	docker run  \
		--cap-add SYSLOG \
		--restart unless-stopped \
		-v /srv/z/logs:/var/logs \
		-p 514:514 \
		-p 514:514/udp \
		-p 20514:20514 \
		--name logwriter \
		sneak/logwriter

